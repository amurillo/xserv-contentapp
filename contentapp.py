import webapp


class ContentApp(webapp.webApp):

    # Creación de un diccionario con el contenido
    content = {'/': 'Main resource',
               '/page1': 'Page 1',
               '/page2': 'Page 2',
               '/page3': 'Page 3',
               '/page4': 'Page 4',
               '/page5': 'Page 5'}

    def parse(self, request):
        # Nos interesa obtener el recurso solicitado
        resource = request.split(' ', 2)[1]
        return resource

    def process(self, resource):
        # Comprobamos si el recurso solicitado está en content
        if resource in self.content.keys():
            returnCode = '200 OK'
            htmlAnswer = "<html><body>" \
                         + self.content[resource] + \
                         "</body></html>"
        else:
            returnCode = '404 Not Found'
            htmlAnswer = "Resource not found"
        return returnCode, htmlAnswer


if __name__ == '__main__':
    testContentApp = ContentApp("", 1234)
